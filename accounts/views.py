from django.contrib import messages
from django.shortcuts import render, redirect
from accounts.form import LoginForm, SignUpForm
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import re
from tracker import settings
from django.core.mail import EmailMessage,send_mail
from django .contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes,force_str
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from accounts.tokens import generate_token




# Create your views here.
# creating login form
def login_form(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user=user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


# creating a logout function
def logout_form(request):
    logout(request)
    return redirect("login")

# with this you can easily get the form from the class form.Forms

# def signup_form(request):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data["username"]
#             password = form.cleaned_data["password"]
#             email = form.cleaned_data.get("email")
#             first_name = form.cleaned_data['first_name']
#             last_name = form.cleaned_data['last_name']
#             password_confirmation = form.cleaned_data["password_confirmation"]
#             if password == password_confirmation:
#                 User.objects.create_user(
#                     username=username, password=password,email=email,first_name=first_name,last_name=last_name
#                 )
#                 return redirect("login")
#     else:
#         form = SignUpForm()
#     context = {"form": form}
#     return render(request, "registration/signup.html", context)

def signup_form(request):

    if request.method == "POST":
        first_name = request.POST.get("fname")
        last_name =request.POST.get("lname")
        username = request.POST.get("username")
        email = request.POST.get("email")
        password = request.POST.get("password")
        password_confirmation = request.POST.get("password_confirmation")

        if User.objects.filter(username=username):
            message = messages.error(request, " username has been used " )
            return redirect("signup")

        if User.objects.filter(email=email):
            message = messages.error(request, " email has been used " )
            return redirect("signup")

        if password != password_confirmation:
            message = messages.error(request, "password and password confirmation are not match " )
            return redirect("signup")

        if len(password) < 8:
            messages.error(request, "the password is les then 8 character")
            return redirect("signup")

        # to check if there is a upercase letter in the password
        if not re.search(r'[A-Z]', password):
            messages.error(request, "the password has no upercase letter")
            return redirect("signup")

        user = User.objects.create_user(username=username, password=password,email=email)
        user.first_name = first_name
        user.last_name = last_name
        user.is_active = False
        user.save()

        # creatign a welcome email

        subject = "welcome to myProject!"
        message = "Hello " + user.first_name + " welcome to myProject and thankyou for visiting our website "
        from_email = settings.EMAIL_HOST_USER
        to_list = [user.email]
        send_mail(subject, message,from_email,to_list, fail_silently=False)
        print(force_bytes(user.pk),"-----------------------user.pk-----------------------")

        # email Addres confirmation Email
        current_site = get_current_site(request)
        email_subject = "confirm your email"
        message2 = render_to_string(
            'email_confirmation.html',{
            'name':user.first_name,
            'domain': current_site.domain,
            'uid':urlsafe_base64_encode(force_bytes(user.pk)),
            'token': generate_token.make_token(user)}
        )
        email = EmailMessage(email_subject, message2,settings.EMAIL_HOST_USER,[user.email])
        email.fail_silently = False
        email.send()

        return redirect("login")

    return render(request, "registration/signup.html")

def activate(request,uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, user.DoesNotExist):
        user = None

    if user is not None and generate_token.check_token(user,token):
        user.is_active = True
        user.save()
        login(request,user)
        return redirect("list_projects")
    else:
        return render(request,"activation_failed.html", {})
