from django.urls import path
from accounts.views import login_form, logout_form, signup_form, activate

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", logout_form, name="logout"),
    path("signup/", signup_form, name="signup"),
    path("activate/<str:uidb64>/<str:token>/",activate, name="activate")
]
