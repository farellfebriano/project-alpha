from django.contrib import admin
from tasks.models import Task


# Register your models here.
@admin.register(Task)
class Task_admin(admin.ModelAdmin):
    list_display = ["name", "id", "project"]

    class Meta:
        fields = (
            "name",
            " start_date",
            "due_date",
            " is_completed",
            "project",
            "assignee",
            "id",
        )
