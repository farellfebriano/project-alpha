from django.shortcuts import render
from tasks.form import CreateTask
from django.shortcuts import redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required


def create_task(request):
    print(request.user, "-------------------request.user--------")
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            user = form.save(False)
            print(user, "---------------user---------")
            user.assignee = request.user
            user.save()
            return redirect("show_my_tasks")
    else:
        form = CreateTask()
    context = {"form": form}
    return render(request, "create_task.html", context)


@login_required()
def show_my_task(request):
    form = Task.objects.filter(assignee=request.user)
    context = {"show_my_task": form}
    return render(request, "show_my_task.html", context)


def delet_task(request, id):
    form = Task.objects.get(id=id)
    form.delete()
    return redirect("show_my_tasks")


def update_task(request, id):
    post = Task.objects.get(id=id)
    if request.method == "POST":
        form = CreateTask(request.POST, instance=post)
        if form.is_valid():
            user = form.save(False)
            user.assignee = request.user
            user.save()
            return redirect("show_my_tasks")
    else:
        form = CreateTask(instance=post)
    context = {"form": form}
    return render(request, "update_task.html", context)
