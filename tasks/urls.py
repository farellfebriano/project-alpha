from django.urls import path
from tasks.views import create_task, show_my_task, delet_task, update_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_task, name="show_my_tasks"),
    path("delet/<int:id>/", delet_task, name="delet_task"),
    path("update/<int:id>/", update_task, name="update_task"),
]
