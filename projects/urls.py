from django.urls import path
from projects.views import (
    list_projects,
    list_task,
    create_project,
    updating_project,
    searched_bar
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", list_task, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/update/", updating_project, name="updating_project"),
    path("search/", searched_bar, name= "searched_bar")
]
