from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.form import ProjectForm
from tasks.models import Task


@login_required
def list_projects(request):
    print()
    form = Project.objects.filter(owner=request.user)
    context = {"list_project": form}
    return render(request, "projects.html", context)


def list_task(request, id):
    form = Project.objects.get(id=id)
    context = {
        "list_task": form,
    }
    return render(request, "show_project.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid:
            user = form.save(False)
            user.owner = request.user
            user.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "create_project.html", context)


def updating_project(request, id):
    post = Project.objects.get(id=id)
    print(post, "-------------------------POST----------")
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm(instance=post)
    context = {"form": form}
    return render(request, "update_project.html", context)


def searched_bar(request):
    if request.method  == "POST":
        search = request.POST.get("searched")
        form_project = Project.objects.filter(name__icontains = search)
        form_task = Task.objects.filter(name__icontains = search)
        context = {
            "form_task":form_task,
            "form_project":form_project,
            "search":search,
        }
        return render(request, "searched_bar.html", context)
    else:
         return render(request, "searched_bar.html", {})
