from django.contrib import admin
from projects.models import Project

# Register your models here.


@admin.register(Project)
class Project_admin(admin.ModelAdmin):
    list_display = ["name", "id"]

    class Meta:
        fields = ("name", "description", "owner")
